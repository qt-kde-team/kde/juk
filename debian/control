Source: juk
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Pino Toscano <pino@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               cmake (>= 3.16~),
               extra-cmake-modules (>= 5.240.0~),
               gettext,
               libkf6completion-dev (>= 5.240.0~),
               libkf6config-dev (>= 5.240.0~),
               libkf6coreaddons-dev (>= 5.240.0~),
               libkf6crash-dev (>= 5.240.0~),
               libkf6dbusaddons-dev (>= 5.240.0~),
               libkf6doctools-dev (>= 5.240.0~),
               libkf6globalaccel-dev (>= 5.240.0~),
               libkf6i18n-dev (>= 5.240.0~),
               libkf6iconthemes-dev (>= 5.240.0~),
               libkf6jobwidgets-dev (>= 5.240.0~),
               libkf6kio-dev (>= 5.240.0~),
               libkf6notifications-dev (>= 5.240.0~),
               libkf6statusnotifieritem-dev (>= 5.240.0~),
               libkf6textwidgets-dev (>= 5.240.0~),
               libkf6wallet-dev (>= 5.240.0~),
               libkf6widgetsaddons-dev (>= 5.240.0~),
               libkf6windowsystem-dev (>= 5.240.0~),
               libkf6xmlgui-dev (>= 5.240.0~),
               libopusfile-dev,
               libphonon4qt6-dev (>= 4:4.6.60~),
               libphonon4qt6experimental-dev,
               libqt5svg5-dev (>= 5.15.2~),
               libtag-dev (>= 1.6),
               pkgconf,
               qt6-base-dev,
               qt6-svg-dev,
Standards-Version: 4.7.0
Homepage: https://juk.kde.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/juk
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/juk.git
Rules-Requires-Root: no

Package: juk
Section: sound
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends},
Suggests: k3b,
Description: music jukebox / music player
 JuK is a powerful music player capable of managing a large music collection.
 .
 Some of JuK's features include:
  * Music collection, playlists, and smart playlists
  * Tag editing support, including the ability to edit multiple files at once
  * Tag-based music file organization and renaming
  * CD burning support using k3b
  * Album art using Google Image Search
 .
 This package is part of the KDE multimedia module.
